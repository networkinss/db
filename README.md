# database mysql postgresql

## Docker instances for running either MySQL or PostgreSQL database

### Requirements
docker
docker-compose 

### Usage
Execute the start.sh script to start database either MySQL and phpmyadmin or PostgreSQL with pgadmin.

### MySQL
The docker-compose.yaml will create a docker volume with name: mysql-data

Database data will be stored inside the created docker volume.
It will store the data in the folder "dbdata".

If the volume is making a problem, you can try following steps:
docker volume prune
docker volume rm mysql_mysql-data

If localhost does not connect, you can retrieve IP from docker instance.
Execute 
docker container ls
to get the container ID for the MySQL running container.
Execute
docker inspect –format ‘{{ .NetworkSettings.IPAddress }}’ <containerid>
to see the IP address.

It might not run together with vpn services.


### PostgreSQL
No adjustments so far yet.





